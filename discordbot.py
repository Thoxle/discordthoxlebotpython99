#time to outline everything
#imports

import discord
import os
import random
import requests
import pprint
from bs4 import BeautifulSoup

#main variables we're working with



from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

#GUILD = os.getenv('DISCORD_GUILD')
#client = discord.Client()
#GUILD = client.get_guild(811393634440183818)

#loading intents to pull members from server
intents = discord.Intents.default()
intents.members = True
client = discord.Client(intents=intents)

GUILD = client.get_guild(811393634440183818)

#page that will be used to pull quote requests
URL = 'https://theninenine.com/quotes/top/'

#request HMTL from page I can print the page but I cannot get beautiful soup content to print
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')

#shows that you are connected
@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

#Shows guild
@client.event
async def on_ready():
    for guild in client.guilds:
        if guild.name == GUILD:
            break
    print(
        f'{client.user} is connected to the following guild:\n'
        f'{guild.name}(id: {guild.id})'
    )

    #print hmtl content is not working
    #results = soup.find(id='footer')
    #print(results.prettify())
    #prints members
    members = '\n - '.join([member.name for member in guild.members])
    print(f'Guild Members:\n - {members}')


#quotes and responding to 99!
@client.event
async def on_message(message):
    if message.author == client.user:
        return

    brooklyn_99_quotes = ['I\'m the human form of the  ^=^r  emoji.','Bingpot!',
        ('Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]

    if message.content == '99!':
        response = random.choice(brooklyn_99_quotes)
        await message.channel.send(response)




client.run(TOKEN)
